#!/bin/bash

#check if user/makefile passed name of the tested file, if not, use main
if [ -z "$1" ]; then
	file="main"
else
	file="$1"
fi



UNDERLINE='\033[4m'
BOLD='\033[1m'
GREEN='\033[0;32m'
RED='\033[0;31m'
URED='\033[4;31m'   #underline red
UGREEN='\033[4;32m'   #underline green
NC='\033[0m' # No Color
function error(){
	echo -e "${RED}ERROR: $1${NC}"
}

function horizontal_line(){
	printf %"$COLUMNS"s | tr " " "-"
}



#print info
echo -e "${BOLD}Testing file '${NC}$file${BOLD}'${NC}"
echo ""

#iterate over testing files in data directory
for file_out in data/*.out; do
	filename="${file_out%.*}"   #filename of testing file (without extension)
	file_in="${filename}.in"   #filename of testing input file (.in)
	file_err="${filename}.err"   #stderr file

	if ! [ -f "${file_in}" ]; then   #for files of type pub00.A.out
		file_in="${file_in%.*}"   #remove .in extension
		file_in="${file_in%.*}.in"   #remove another extension and add .in extension

		if ! [ -f "${file_in}" ]; then   #for files of type pub00-a.out
			file_in="${file_in%.*}"   #remove .in extension
			file_in="${file_in%-*}.in"   #remove word after `-` and add .in extension
		fi
	fi

	if [ -f "${file_in}" ] && [ -f "${file_out}" ]; then   #check if bot files exist
		echo -e "${BOLD}Checking for '$file_in' and '$file_out'...${NC}"

		#run code
		valgrind -s --leak-check=full --error-exitcode=125 --log-file=valgrind_out.tmp ./$file < "$file_in" > out.tmp 2>err.tmp   #pass input file content and save output to the out.tmp

		#---------------valgrind---------------
		if [[ $? -eq 125 ]]; then   #if return code is 125. valgrind found memory error/leak
			echo -e "	${UNDERLINE}memory ${URED}Failed${NC}:"
			cat valgrind_out.tmp   #print diff output
			echo ""
		else
			echo -e "	memory ${GREEN}Passed.${NC}"
			echo ""
		fi
		#---------------valgrind---------------

		#---------------stdout---------------
		#check output and expected output using diff command
		diff_out="$(diff -y -W 100 "$file_out" out.tmp)"   #-y means print in columns, -W 100 is terminal width 100 columns
		if [[ $? -eq 0 ]]; then   #if return code is zero, files are same
			echo -e "	stdout ${GREEN}Passed.${NC}"
			echo ""
		else
			echo -e "	${UNDERLINE}stdout ${URED}Failed${NC}:"
			echo "$diff_out"   #print diff output
			echo ""
		fi
		#---------------stdout---------------

		#---------------stderr---------------
		if [ -f "${file_err}" ]; then
			#check output and expected output using diff command
			diff_out="$(diff -y -W 100 "$file_err" err.tmp)"   #-y means print in columns, -W 100 is terminal width 100 columns
			if [[ $? -eq 0 ]]; then   #if return code is zero, files are same
				echo -e "	stderr ${GREEN}Passed.${NC}"
				echo ""
			else
				echo -e "	${UNDERLINE}stderr ${URED}Failed${NC}:"
				echo "$diff_out"   #print diff output
				echo ""
			fi
		fi
		#---------------stderr---------------

		horizontal_line   #draw dashed horizontal line

	else
		error "File ${file_out} doesn't have ${file_in}!"
	fi
done


rm -f out.tmp   #remove temporary out.tmp file
rm -f err.tmp   #remove temporary err.tmp file
rm -f valgrind_out.tmp
