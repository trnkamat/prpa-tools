#!/bin/bash

#-----------print functions----------
BOLD='\033[1m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color
function error(){
	echo -e "${RED}ERROR: $1${NC}"
}

function status(){
	echo -n -e "${BOLD}$1${NC}.. "
}

function ok(){
	echo -e "${GREEN}OK${NC}"
}
#-----------print functions----------


#arguments check
if [ -z "$1" ]; then
	echo -e "${RED}Please provide HW name!${NC} for example HW01"
	exit
fi



hw="$1"   #hw (homework) name
hw_lower="$(echo $hw | tr '[:upper:]' '[:lower:]')"   #hw in lowercase, for prpa git
tools_dir="prpa-tools"   #location of prpa-tools - this script, makefile, etc.
prpa_git="prpa"    #prpa git repository


#check wroking directory of this script and eventually go one level back
if [ "$(basename $(pwd))" == "$tools_dir" ]; then
	echo "Changing dir, 'cd ..'"
	cd ..
fi


#create HW directory
status "Creating directory '$hw'"
mkdir -p "$hw" && ok

#copy main.c template from prpa-tools
status "Creating main.c template"
if [ -f "$hw/main.c" ]; then   #we don't want to overwrite existing main.c
	error "file already exists"
else
	cp "$tools_dir/main.c.template" "$hw/main.c" && ok
fi

#copy makefile and program test script
status "Copying makefile and test"
cp "$tools_dir/makefile" "$hw" && \
cp "$tools_dir/code_test.sh" "$hw" && ok

#create directory for testing data
status "Creating testing data directory"
mkdir -p "$hw/data" && ok

#update prpa git repository
status "Pulling prpa git"
cd "$prpa_git" && git pull && cd .. && ok

#copy testing data from prpa git to the new HW dir
status "Trying to obtain testing data"
cp -r "prpa/homeworks/$hw_lower/data/" "$hw/" && ok
