CFLAGS+=  -pedantic -Wall -std=c99 -O3 -g
HW=$(shell basename ${PWD})

main: main.c
	${CC} ${CFLAGS} main.c -o main

test:
	bash code_test.sh

zip:
	zip ${HW}.zip main.c

clean:
	rm -f *.o
	rm -f main
	rm -f ${HW}.zip

.PHONY: clean zip
